
import Foundation
import UIKit
import Localize_Swift
import SnapKit
import Material

public protocol BaseLayoutDelegate {
    func retry()
}

public class BaseLayout {
    
    var superview: UIView!
    var delegate: BaseLayoutDelegate!
    
    lazy var somethingWentWrongLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "somethingWentWrong".localized()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textAlignment = .center
        label.isHidden = true
        return label
    }()
    
    lazy var retryButton: RaisedButton = {
        let button = RaisedButton(title: "retry".localized(), titleColor: .white)
        button.backgroundColor = UIColor.AppColors.gray
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 0.5)
        button.addTapGesture { recognizer in
            self.delegate.retry()
        }
        button.isHidden = true
        return button
    }()
    
    /**
     Initializer that initialize the superView of the layout and set its background.
     - Parameter superview: The main container of the screen
     */
    init(superview: UIView, delegate: BaseLayoutDelegate) {
        self.superview = superview
        self.delegate = delegate
        superview.backgroundColor = .white
        setupErrorView()
    }
    
    /**
     Initializer that initialize the superView of the layout and set its background.
     - Parameter superview: The main container of the screen
     */
    init(superview: UIView) {
        self.superview = superview
        superview.backgroundColor = .white
    }
    
    func setupErrorView() {
        superview.addSubviews([somethingWentWrongLabel, retryButton])
        retryButton.snp.makeConstraints { (maker) in
            maker.center.equalTo(superview)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 60))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        somethingWentWrongLabel.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(retryButton.snp.top).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.leading.trailing.equalTo(superview)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
    }
    
    public func showErrorViews() {
        retryButton.isHidden = false
        somethingWentWrongLabel.isHidden = false
        superview.bringSubviewToFront(retryButton)
        superview.bringSubviewToFront(somethingWentWrongLabel)
    }
    
    public func hideErrorViews() {
        retryButton.isHidden = true
        somethingWentWrongLabel.isHidden = true        
    }
}
