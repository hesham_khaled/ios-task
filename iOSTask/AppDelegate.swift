//
//  AppDelegate.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Localize_Swift
import Material
import AlamofireNetworkActivityIndicator

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    public static var instance: AppDelegate!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {        
        AppDelegate.instance = self
        NetworkActivityIndicatorManager.shared.isEnabled = true
        NetworkActivityIndicatorManager.shared.startDelay = 0.2
        NetworkActivityIndicatorManager.shared.completionDelay = 0.5
        startApplication()
        
        return true
    }
    
    /**
     This method set the allignment of all used design components according to current language and set the root viewController according to loggedIn or not.
     */
    func startApplication() {
        
        switch Localize.currentLanguage() {
        case "ar":
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UILabel.appearance().semanticContentAttribute = .forceRightToLeft
            FlatButton.appearance().semanticContentAttribute = .forceRightToLeft
            FlatButton.appearance().contentHorizontalAlignment = .right
            RaisedButton.appearance().semanticContentAttribute = .forceRightToLeft
            RaisedButton.appearance().contentHorizontalAlignment = .right
            Button.appearance().contentHorizontalAlignment = .right
            Button.appearance().semanticContentAttribute = .forceRightToLeft
            UIButton.appearance().semanticContentAttribute = .forceRightToLeft
            UIImageView.appearance().semanticContentAttribute = .forceRightToLeft
            UITableView.appearance().semanticContentAttribute = .forceRightToLeft
            UICollectionView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
            ErrorTextField.appearance().semanticContentAttribute = .forceRightToLeft
            TextField.appearance().semanticContentAttribute = .forceRightToLeft
            Switch.appearance().semanticContentAttribute = .forceRightToLeft
            UITableViewCell.appearance().semanticContentAttribute = .forceRightToLeft
            UICollectionViewCell.appearance().semanticContentAttribute = .forceRightToLeft
            UISearchBar.appearance().semanticContentAttribute = .forceRightToLeft
            UIProgressView.appearance().semanticContentAttribute = .forceRightToLeft
            
            
            break
            
        case "en":
            Switch.appearance().semanticContentAttribute = .forceLeftToRight
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UILabel.appearance().semanticContentAttribute = .forceLeftToRight
            FlatButton.appearance().semanticContentAttribute = .forceLeftToRight
            FlatButton.appearance().contentHorizontalAlignment = .left
            RaisedButton.appearance().contentHorizontalAlignment = .left
            RaisedButton.appearance().semanticContentAttribute = .forceLeftToRight
            Button.appearance().contentHorizontalAlignment = .left
            Button.appearance().semanticContentAttribute = .forceLeftToRight
            UIButton.appearance().semanticContentAttribute = .forceLeftToRight
            UIImageView.appearance().semanticContentAttribute = .forceLeftToRight
            UITableView.appearance().semanticContentAttribute = .forceLeftToRight
            UICollectionView.appearance().semanticContentAttribute = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
            UITabBar.appearance().semanticContentAttribute = .forceLeftToRight
            ErrorTextField.appearance().semanticContentAttribute = .forceLeftToRight
            TextField.appearance().semanticContentAttribute = .forceLeftToRight
            UITableViewCell.appearance().semanticContentAttribute = .forceLeftToRight
            UICollectionViewCell.appearance().semanticContentAttribute = .forceLeftToRight
            UISearchBar.appearance().semanticContentAttribute = .forceLeftToRight
            UIProgressView.appearance().semanticContentAttribute = .forceLeftToRight
            break
            
        default:
            break
        }
        
        let navigationController = UINavigationController()
        
        navigationController.navigationBar.setStyle(style: .solid, tintColor: UIColor.white, forgroundColor: .black)
        
        if let loggedIn = Defaults[.isLoggedIn], loggedIn {
            navigationController.navigationBar.isHidden = true
            self.window?.rootViewController = navigationController
            navigationController.pushViewController(RestaurantsVC.buildVC(), animated: false)
        } else {
            self.window?.rootViewController = navigationController
            navigationController.pushViewController(LoginVC.buildVC(), animated: false)
        }
        
    }
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

