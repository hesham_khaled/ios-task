//
//  OrderDetailsVC.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit

class OrderDetailsVC: BaseVC {

    @IBOutlet weak var backIcon: UIImageView!
    @IBOutlet weak var productsTableView: UITableView!
    
    var products: [Product]!
    
    public class func buildVC(products: [Product]) -> OrderDetailsVC {
        let storyboard = UIStoryboard(name: "OrderDetails", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "OrderDetailsVC")
        (controller as! OrderDetailsVC).products = products
        return controller as! OrderDetailsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productsTableView.dataSource = self
        productsTableView.delegate = self
        productsTableView.reloadData()
        
        backIcon.addTapGesture { (_) in
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension OrderDetailsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:OrderDetailsCell = productsTableView.dequeueReusableCell(withIdentifier: OrderDetailsCell.identifier, for: indexPath) as! OrderDetailsCell
        
        cell.selectionStyle = .none
        cell.product = self.products.get(at: indexPath.row)
        cell.populateData()
        
        return cell
    }
}
