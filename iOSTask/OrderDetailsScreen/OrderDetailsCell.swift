//
//  OrderDetailsCell.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit

class OrderDetailsCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    public static let identifier = "OrderDetailsCell"
    
    var product: Product!
    
    public func populateData() {
        self.productImageView.af_setImage(withURL: URL(string: product.image)!)
        self.productNameLabel.text = self.product.name
        self.productQuantityLabel.text = "Quantity: \(self.product.quantity!)"
        self.productPriceLabel.text = "Price: \(self.product.price!)$"
    }
}
