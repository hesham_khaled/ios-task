//
//  RestaurantsPresenter.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public protocol RestaurantsView : class {
    func getRestaurantsSuccess(restaurants: [Restaurant])
    func operationFailed(errorMessage: String)
}

public class RestauratnsPresenter {
    fileprivate weak var restaurantsView : RestaurantsView?
    fileprivate let restaurantsRepository : RestaurantsRepository
    
    init(repository: RestaurantsRepository) {
        self.restaurantsRepository = repository
        self.restaurantsRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : RestaurantsView) {
        restaurantsView = view
    }
}

extension RestauratnsPresenter {
    public func getRestaurants() {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.restaurantsRepository.getRestaurants()
        } else {
            self.operationFailed(errorMessage: "No internet connection. Please check your connection and try again.")
        }
    }
}

extension RestauratnsPresenter: RestaurantsPresenterDelegate {
    public func getRestaurantsSuccess(restaurants: [Restaurant]) {
        UiHelpers.hideLoader()
        self.restaurantsView?.getRestaurantsSuccess(restaurants: restaurants)
    }
    
    public func operationFailed(errorMessage: String) {
        self.restaurantsView?.operationFailed(errorMessage: errorMessage)
    }
    
    
}
