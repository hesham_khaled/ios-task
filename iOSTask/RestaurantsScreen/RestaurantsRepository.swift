//
//  RestaurantsRepository.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import Localize_Swift

public protocol RestaurantsPresenterDelegate {
    func getRestaurantsSuccess(restaurants: [Restaurant])
    func operationFailed(errorMessage: String)
}

public class RestaurantsRepository {
    var delegate: RestaurantsPresenterDelegate!
    
    public func setDelegate(delegate: RestaurantsPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getRestaurants() {
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "getResturants?langu=\(Localize.currentLanguage())")!, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if let status = json["status"] as? Int, status == 200 {
                        if let restaurantsJsonArray = json["return"] as? [Dictionary<String,AnyObject>] {
                            var restaurants: [Restaurant] = []
                            for restaurantDic in restaurantsJsonArray {
                                restaurants.append(Restaurant.getInstance(dictionary: restaurantDic))
                            }
                            
                            self.delegate.getRestaurantsSuccess(restaurants: restaurants)
                        } else {
                            self.delegate.operationFailed(errorMessage: "Error in parsing")
                        }
                    } else {
                        self.delegate.operationFailed(errorMessage: json["message"] as! String)
                    }
                }
            } else {
                self.delegate.operationFailed(errorMessage: "Somthing went wrong. Please try again")
            }
        }
    }
}
