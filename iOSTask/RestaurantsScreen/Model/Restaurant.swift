//
//  Restaurant.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class Restaurant {
    
    var id: String!
    var name: String!
    var image: String!
    var location: String!
    var type: String!
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["rest_id"] = id
        dictionary["rest_name"] = name
        dictionary["rest_img"] = image
        dictionary["rest_location"] = location
        dictionary["rest_type"] = type
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Restaurant {
        
        let restaurant = Restaurant()
        
        restaurant.id =  dictionary["rest_id"] as? String
        restaurant.name = dictionary["rest_name"] as? String
        restaurant.image = dictionary["rest_img"] as? String
        restaurant.location = dictionary["rest_location"] as? String
        restaurant.type = dictionary["rest_type"] as? String
        return restaurant
    }
}
