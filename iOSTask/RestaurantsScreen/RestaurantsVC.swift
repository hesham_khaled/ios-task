//
//  RestaurantsVC.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import MapKit
import Material

class RestaurantsVC: BaseVC {
    
    
    @IBOutlet weak var showAsListButton: FlatButton!
    @IBOutlet weak var showAsGridButton: FlatButton!
    @IBOutlet weak var restaurantsCollectionView: UICollectionView!
    @IBOutlet weak var restaurantsTableView: UITableView!
    
    var presenter: RestauratnsPresenter!
    var restaurants: [Restaurant]!
    
//    var layout: RestaurantsLayout!
    let locationManager = CLLocationManager()
    
    public static func buildVC() -> RestaurantsVC {
        let storyboard = UIStoryboard(name: "Restaurants", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RestaurantsVC")
        
        return controller as! RestaurantsVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        
        showAsListButton.contentHorizontalAlignment = .center
        showAsGridButton.contentHorizontalAlignment = .center
        
        restaurantsTableView.dataSource = self
        restaurantsTableView.delegate = self
        restaurantsTableView.rowHeight = UITableView.automaticDimension
        restaurantsTableView.estimatedRowHeight = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12)
        
        restaurantsCollectionView.dataSource = self
        restaurantsCollectionView.delegate = self
        
        restaurantsCollectionView.showsHorizontalScrollIndicator = false
        restaurantsCollectionView.contentSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 100), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 300))
        if let layout = restaurantsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .vertical
            layout.itemSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 40), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 40))
        }
        
        presenter = Injector.provideRestaurantsPresenter()
        presenter.setView(view: self)
        presenter.getRestaurants()
    }
    
    @IBAction func showAsListClicked(_ sender: Any) {
        restaurantsCollectionView.isHidden = true
        restaurantsTableView.isHidden = false
    }
    
    @IBAction func showAsGridClicked(_ sender: Any) {
        restaurantsCollectionView.isHidden = false
        restaurantsTableView.isHidden = true
    }
    
    
}

extension RestaurantsVC: RestaurantsView {
    func getRestaurantsSuccess(restaurants: [Restaurant]) {
        self.restaurants = restaurants
        restaurantsTableView.reloadData()
        restaurantsCollectionView.reloadData()
    }
    
    func operationFailed(errorMessage: String) {
        self.view.makeToast(errorMessage)
    }
}

extension RestaurantsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = self.restaurants {
            return self.restaurants.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantTableViewCell.identifier, for: indexPath) as! RestaurantTableViewCell
        cell.selectionStyle = .none
        cell.index = indexPath.row
        cell.delegate = self
        cell.restaurant = self.restaurants.get(at: indexPath.row)
        cell.populateData()
        
        return cell
    }
}

extension RestaurantsVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let _ = self.restaurants {
            return self.restaurants.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RestaurantCollectionViewCell.identifier, for: indexPath) as! RestaurantCollectionViewCell
        cell.index = indexPath.row
        cell.delegate = self
        cell.restaurant = self.restaurants.get(at: indexPath.row)
        cell.populateData()
        return cell
    }
}

extension RestaurantsVC: RestaurantCellDelegate {
    func openOrdersScreen(index: Int) {
        let selectedRestaurant = self.restaurants.get(at: index)
        
        self.navigator.navigateToOrdersScreen(restaurant: selectedRestaurant!)
    }
}

