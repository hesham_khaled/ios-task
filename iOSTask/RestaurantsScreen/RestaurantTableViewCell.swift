//
//  RestaurantTableViewCell.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import AlamofireImage
import MapKit

class RestaurantTableViewCell: UITableViewCell {

    public static let identifier = "restaurantTableViewCell"

    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantTypeLabel: UILabel!
    @IBOutlet weak var showOnMapLabel: UILabel!
    
    
    var restaurant: Restaurant!
    var index: Int!
    var delegate: RestaurantCellDelegate!
    
    public func populateData() {
        self.restaurantImageView.af_setImage(withURL: URL(string: restaurant.image)!)
        self.restaurantNameLabel.text = self.restaurant.name
        self.restaurantTypeLabel.text = self.restaurant.type
        
        self.contentView.addTapGesture { (_) in
            self.delegate.openOrdersScreen(index: self.index)
        }
        
        self.showOnMapLabel.addTapGesture { (_) in
            
            let latitude = self.restaurant.location.components(separatedBy: ",")[0].toDouble()
            let longitude = self.restaurant.location.components(separatedBy: ",")[1].toDouble()
            
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitude!, longitude!)
            let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = self.restaurant.name
            mapItem.openInMaps(launchOptions: options)
        }
    }
}
