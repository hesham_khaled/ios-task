//
//  Navigator.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public class Navigator {
    
    var navigationController: UINavigationController!
    
    public init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    public func navigateToRestaurantsScreen() {
        let vc = RestaurantsVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToOrdersScreen(restaurant: Restaurant) {
        let vc = OrdersVC.buildVC(restaurant: restaurant)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToOrderDetailsScreen(products: [Product]) {
        let vc = OrderDetailsVC.buildVC(products: products)
        self.navigationController.pushViewController(vc, animated: true)
    }
}
