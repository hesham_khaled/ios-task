//
//  DefaultsKeys.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    static let user = DefaultsKey<Dictionary<String, Any>?>("user")
    static let isLoggedIn = DefaultsKey<Bool?>("isLoggedIn")
        
}
