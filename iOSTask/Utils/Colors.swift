//
//  Colors.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    enum AppColors {
        
        static let primaryColor = UIColor.black
        static let lightGray = UIColor(hexString: "#f8f8f8")!
        static let gray = UIColor(hexString: "#d6d6d6")!
        static let darkGray = UIColor(hexString: "#6f6f6f")!
        static let red = UIColor(hexString: "#d0112b")!
        static let darkRed = UIColor(hexString: "#7c000b")!
        static let green = UIColor(hexString: "#007b06")!
        static let yellow = UIColor(hexString: "#FAC900")!
        static let offWhite = UIColor(hexString: "#FAEBD7")
        static let alphaBlack = UIColor(hexString: "#000000")?.withAlphaComponent(0.8)
    }
}
