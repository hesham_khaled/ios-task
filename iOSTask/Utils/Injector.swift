//
//  Injector.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
public class Injector {
    public class func provideLoginPresenter() -> LoginPresenter {
        return LoginPresenter(repository: Injector.provideLoginRepository())
    }
    
    public class func provideLoginRepository() -> LoginRepository {
        return LoginRepository()
    }
    
    public class func provideRestaurantsPresenter() -> RestauratnsPresenter {
        return RestauratnsPresenter(repository: Injector.provideRestaurantsRepository())
    }
    
    public class func provideRestaurantsRepository() -> RestaurantsRepository {
        return RestaurantsRepository()
    }
    
    public class func provideOrdersPresenter() -> OrdersPresenter {
        return OrdersPresenter(repository: Injector.provideOrdersRepository())
    }
    
    public class func provideOrdersRepository() -> OrdersRepository {
        return OrdersRepository()
    }
}
