//
//  User.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class User {
    var id: Int!
    var age: Int!
    var gender: String!
    var mobile: String!
    var password: String!
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["id"] = id
        dictionary["age"] = age
        dictionary["gender"] = gender
        dictionary["mobile"] = mobile
        dictionary["password"] = password
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> User {
        
        let user = User()
        
        user.id =  dictionary["id"] as? Int
        user.gender = dictionary["gender"] as? String
        user.age = dictionary["age"] as? Int
        user.mobile = dictionary["mobile"] as? String
        user.password = dictionary["password"] as? String
        return user
    }
}
