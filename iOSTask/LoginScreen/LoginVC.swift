//
//  LoginVC.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import Material

class LoginVC: BaseVC {
    
    var presenter: LoginPresenter!
    var user: User!
    
    @IBOutlet weak var mobileNumberField: ErrorTextField!
    @IBOutlet weak var passwordField: ErrorTextField!
    @IBOutlet weak var loginButton: RaisedButton!
    
    
    public static func buildVC() -> LoginVC {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginVC")

        return controller as! LoginVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.contentHorizontalAlignment = .center
        presenter = Injector.provideLoginPresenter()
        presenter.setView(view: self)
        
    }
    
    @IBAction func login(_ sender: Any) {
        if (self.mobileNumberField.text?.isEmpty)! || (self.passwordField.text?.isEmpty)!{
            self.view.makeToast("Please fill empty fields")
        } else if !(self.mobileNumberField.text?.isNumber())! {
            self.view.makeToast("The entered phone number is not valid")
        } else {
            self.presenter.login(email: self.mobileNumberField.text!, password: self.passwordField.text!)
        }
    }
    
    func goToRestaurantsScreen() {
        if navigator == nil {
            navigator = Navigator(navController: self.navigationController!)
        }
        
        navigator.navigateToRestaurantsScreen()
    }
}

extension LoginVC : LoginView {
    
    func loginSuccess(user: User) {
        
        Defaults[.user] = user.convertToDictionary()
        Defaults[.isLoggedIn] = true
        self.user = user
        
        self.view.makeToast("Logged in success", duration: 1) {
            self.goToRestaurantsScreen()
        }
    }
    
    func loginFailed(errorMessage: String) {
        self.view.makeToast(errorMessage)
    }
}

