//
//  LoginPresenter.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation


public protocol LoginView : class {
    func loginSuccess(user: User)
    func loginFailed(errorMessage: String)
}

public class LoginPresenter {
    fileprivate weak var loginView : LoginView?
    fileprivate let loginRepository : LoginRepository
    
    init(repository: LoginRepository) {
        self.loginRepository = repository
        self.loginRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : LoginView) {
        loginView = view
    }
}

extension LoginPresenter {
    public func login(email: String, password: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.loginRepository.login(email: email, password: password)
        } else {
            self.loginFailed(errorMessage: "No internet connection. Please check your connection and try again.")
        }
    }
}

extension LoginPresenter: LoginPresenterDelegate {
    public func loginSuccess(user: User) {
        UiHelpers.hideLoader()
        loginView?.loginSuccess(user: user)
    }
    
    public func loginFailed(errorMessage: String) {
        UiHelpers.hideLoader()
        loginView?.loginFailed(errorMessage: errorMessage)
    }
}
