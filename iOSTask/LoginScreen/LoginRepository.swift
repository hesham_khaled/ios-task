//
//  LoginRepository.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire

public protocol LoginPresenterDelegate {
    func loginSuccess(user: User)
    func loginFailed(errorMessage: String)
}

public class LoginRepository {
    
    var delegate: LoginPresenterDelegate!
    
    public func setDelegate(delegate: LoginPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func login(email: String, password: String) {
        let headers = ["Content-Type" : "application/json"]
        let parameters = ["mobile" : email, "password" : password, "access_key" : "Gdka52DASWE3DSasWE742Wq", "access_password" : "yH52dDDF85sddEWqPNV7D12sW5e"]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "login")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if let status = json["status"] as? Int, status == 200 {
                        let user = User.getInstance(dictionary: json["return"] as! Dictionary<String, Any>)
                        self.delegate.loginSuccess(user: user)
                    } else {
                        self.delegate.loginFailed(errorMessage: json["message"] as! String)
                    }
                }
            } else {
                self.delegate.loginFailed(errorMessage: "Somthing went wrong. Please try again")
            }
        }
    }
}
