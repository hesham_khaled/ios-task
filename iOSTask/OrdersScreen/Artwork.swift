//
//  Artwork.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import MapKit

class Artwork: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    let index: Int!
    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D, index: Int) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        self.index = index
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
    
    
}
