//
//  Product.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class Product {
    
    var name: String!
    var quantity: Int!
    var price: String!
    var image: String!
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["prod_quantity"] = quantity
        dictionary["prod_name"] = name
        dictionary["prod_image"] = image
        dictionary["prod_price"] = price
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Product {
        
        let product = Product()
        
        product.name = dictionary["prod_name"] as? String
        product.image = dictionary["prod_image"] as? String
        product.quantity = dictionary["prod_quantity"] as? Int
        product.price = dictionary["prod_price"] as? String
        return product
    }
}
