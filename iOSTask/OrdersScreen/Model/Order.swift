//
//  Order.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class Order {
    
    var id: String!
    var userName: String!
    var orderPrice: String!
    var userLocation: String!
    var restaurantName: String!
    var orderDetails: [Product]!
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["order_id"] = id
        dictionary["order_user"] = userName
        dictionary["order_price"] = orderPrice
        dictionary["user_location"] = userLocation
        dictionary["resturant_name"] = restaurantName
        if orderDetails != nil {
            var orderDetailsDicArray = [Dictionary<String, Any>]()
            
            for product in orderDetails {
                orderDetailsDicArray.append(product.convertToDictionary())
            }
            dictionary["order_details"] = orderDetailsDicArray
        }
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Order {
        
        let order = Order()
        
        order.id = dictionary["order_id"] as? String
        order.userName = dictionary["order_user"] as? String
        order.orderPrice = dictionary["order_price"] as? String
        order.userLocation = dictionary["user_location"] as? String
        order.restaurantName = dictionary["resturant_name"] as? String
        
        if let orderDetailsArrayDic = dictionary["order_details"], orderDetailsArrayDic is [Dictionary<String, Any>] {
            var products: [Product] = []
            for dic in orderDetailsArrayDic as! [Dictionary<String, Any>] {
                products.append(Product.getInstance(dictionary: dic))
            }
            order.orderDetails = products
        }
        return order
    }
}
