//
//  OrdersVC.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class OrdersVC: BaseVC, CLLocationManagerDelegate {

    
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var backIcon: UIImageView!
    
    
    var restaurant: Restaurant!
    var orders: [Order]!
    var presenter: OrdersPresenter!
    let locationManager = CLLocationManager()
    var currentLocationLatitude: Double!
    var currentLocationLongitude: Double!
    let regionRadius:CLLocationDistance = 1000000
    
    public static func buildVC(restaurant: Restaurant) -> OrdersVC {
        let storyboard = UIStoryboard(name: "Orders", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "OrdersVC")
        (controller as! OrdersVC).restaurant = restaurant
        return controller as! OrdersVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if navigator == nil {
            navigator = Navigator(navController: self.navigationController!)
        }
        
        mapView.delegate = self
        backIcon.addTapGesture { (_) in
            self.navigationController?.popViewController(animated: true)
        }
        
        DispatchQueue.main.async {
            UiHelpers.showLoader()
            self.locationAuthorization()
        }
        
        presenter = Injector.provideOrdersPresenter()
        presenter.setView(view: self)
        presenter.getOrders(restaurantId: restaurant.id)
    }
    
    func locationAuthorization(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways {
            determineMyCurrentLocation()
            
        } else if (CLLocationManager.authorizationStatus() == .denied) {
            showSettingsAlert()
        } else {
            locationManager.requestWhenInUseAuthorization()
            sleep(UInt32(0.5))
            locationAuthorization()
        }
    }
    
    
    func determineMyCurrentLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        self.currentLocationLatitude = userLocation.coordinate.latitude
        self.currentLocationLongitude = userLocation.coordinate.longitude
        showMyLocationOnTheMap(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        UiHelpers.hideLoader()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }

    func showMyLocationOnTheMap(latitude: Double, longitude: Double) {
        let restaurantLocation = CLLocation(latitude: latitude, longitude: longitude)
        let coordinateRegion = MKCoordinateRegion(center: restaurantLocation.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        let artwork = Artwork(title: "My Location",
                              locationName: restaurant.name,
                              discipline: "Restaurant",
                              coordinate: CLLocationCoordinate2D(latitude:latitude, longitude: longitude), index: -1)
        mapView.addAnnotation(artwork)
    }
    
    func showSettingsAlert() {
        let alert = UIAlertController(title: "Need Authorization", message: "This app is unusable if you don't authorize this app to use your location!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { _ in
            let url = URL(string: UIApplication.openSettingsURLString)!
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showOrdersPlacesOnTheMap() {
        var index: Int = 0

        for order in orders {
            index = index + 1
            let latitude = order.userLocation.components(separatedBy: ",")[0].toDouble()
            let longitude = order.userLocation.components(separatedBy: ",")[1].toDouble()
            
            let artwork = Artwork(title: "Order \(index)",
                                  locationName: order.userName,
                                  discipline: "Order",
                                  coordinate: CLLocationCoordinate2D(latitude:latitude!, longitude: longitude!), index: index)
            mapView.addAnnotation(artwork)
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {

        let orderDetailsAction = UIAlertAction(title: "Show Order Details", style: .default) { (handler) in
            let index = view.tag
            self.navigator.navigateToOrderDetailsScreen(products: (self.orders.get(at: index)?.orderDetails)!)
        }
        
        let showRoutesAction = UIAlertAction(title: "Show Route", style: .default) { (handler) in
            let index = view.tag
            let selectedOrder = self.orders.get(at: index)
            if let  _ = selectedOrder {
                let latitude = selectedOrder?.userLocation.components(separatedBy: ",")[0].toDouble()
                let longitude = selectedOrder?.userLocation.components(separatedBy: ",")[1].toDouble()
                
                self.showRouteOnMap(source: CLLocationCoordinate2D(latitude: self.currentLocationLatitude, longitude: self.currentLocationLongitude), destination: CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!))
            }
        }
        
        let orderDoneAction = UIAlertAction(title: "Order Delivered", style: .default) { (handler) in
            let index = view.tag
            for annotation in mapView.annotations {
                if let title = annotation.title, title == "Order \(index)" {
                    mapView.removeAnnotation(annotation)
                }
            }
        }
        
        let alertController = UiHelpers.createAlertView(title: "Choose Action ...", message: "Choose an action to be done", actions: [orderDetailsAction, showRoutesAction, orderDoneAction])
        
        self.presentVC(alertController)
        
    }
}

extension OrdersVC: OrdersView {
    func getOrdersSuccess(orders: [Order]) {
        self.orders = orders
        showOrdersPlacesOnTheMap()
    }
    
    func operationFailed(errorMessage: String) {
        self.view.makeToast(errorMessage)
    }
}

extension OrdersVC: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Artwork {
            if annotation.title != "My Location" {
                let identifier = "marker"
                var view: MKMarkerAnnotationView
                if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                    as? MKMarkerAnnotationView {
                    dequeuedView.annotation = annotation
                    view = dequeuedView
                } else {
                    view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                    view.canShowCallout = true
                    view.calloutOffset = CGPoint(x: -5, y: 5)
                    view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
                }
                view.tag = annotation.index
                return view
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    func showRouteOnMap(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) {
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: source, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destination, addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .any
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            
            if let _ = error {
                self.view.makeToast((error?.localizedDescription)!)
            } else {
                guard let unwrappedResponse = response else { return }
                
                if (unwrappedResponse.routes.count > 0) {
                    self.mapView.addOverlay(unwrappedResponse.routes[0].polyline)
                    self.mapView.setVisibleMapRect(unwrappedResponse.routes[0].polyline.boundingMapRect, animated: true)
                }
            }
        }
    }
    
    func mapView(_ mapView: MKMapView!, rendererFor overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }
        return MKPolylineRenderer()
    }

}

