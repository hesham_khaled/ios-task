//
//  OrdersRepository.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import Localize_Swift

public protocol OrdersPresenterDelegate {
    func getOrdersSuccess(orders: [Order])
    func operationFailed(errorMessage: String)
}

public class OrdersRepository {
    var delegate: OrdersPresenterDelegate!
    
    public func setDelegate(delegate: OrdersPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getOrders(restaurantId: String) {
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "getOrder?langu=\(Localize.currentLanguage())&restId=\(restaurantId)")!, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if let status = json["status"] as? Int, status == 200 {
                        if let ordersJsonArray = json["return"] as? [Dictionary<String,AnyObject>] {
                            var orders: [Order] = []
                            for orderDic in ordersJsonArray {
                                orders.append(Order.getInstance(dictionary: orderDic))
                            }
                            self.delegate.getOrdersSuccess(orders: orders)
                        } else {
                            self.delegate.operationFailed(errorMessage: "Error in parsing")
                        }
                    } else {
                        self.delegate.operationFailed(errorMessage: json["message"] as! String)
                    }
                }
            } else {
                self.delegate.operationFailed(errorMessage: "Somthing went wrong. Please try again")
            }
        }
    }
}
