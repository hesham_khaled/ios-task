//
//  OrdersPresenter.swift
//  iOSTask
//
//  Created by Hesham Donia on 3/25/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
public protocol OrdersView : class {
    func getOrdersSuccess(orders: [Order])
    func operationFailed(errorMessage: String)
}

public class OrdersPresenter {
    fileprivate weak var ordersView : OrdersView?
    fileprivate let ordersRepository : OrdersRepository
    
    init(repository: OrdersRepository) {
        self.ordersRepository = repository
        self.ordersRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : OrdersView) {
        ordersView = view
    }
}

extension OrdersPresenter {
    public func getOrders(restaurantId: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.ordersRepository.getOrders(restaurantId: restaurantId)
        } else {
            self.operationFailed(errorMessage: "No internet connection. Please check your connection and try again.")
        }
    }
}

extension OrdersPresenter: OrdersPresenterDelegate {
    public func getOrdersSuccess(orders: [Order]) {
        UiHelpers.hideLoader()
        self.ordersView?.getOrdersSuccess(orders: orders)
    }
    
    public func operationFailed(errorMessage: String) {
        self.ordersView?.operationFailed(errorMessage: errorMessage)
    }
    
    
}
